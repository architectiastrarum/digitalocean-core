/*
 * Copyright (c) 2019. Challstrom. All rights reserved.
 */

const got = require('got');
const endpoint = 'domains';
const Global = require('./Global');
const url = Global.url;
const domainsUrl = `${url}/${endpoint}`;
const authorization = `Bearer ${Global.token}`;

class DNS {
    static async getAllDNSRecords(domain, page = 1) {
        const request = await got(`${domainsUrl}/${domain}/records`, {
            json: true,
            query: {
                page: page
            },
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        });
        return request.body;
    }

    static async createDNSRecord(domain, type, name, data, priority, port, ttl=1800, weight, flags, tag) {
        const request = await got(`${domainsUrl}/${domain}/records`, {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            body: {
                type: type,
                name: name,
                data: data,
                priority: priority,
                port: port,
                ttl: ttl,
                weight: weight,
                flags: flags,
                tag: tag
            }
        }).catch((err) => {
            console.error(err);
            console.error({
                type: type,
                name: name,
                data: data,
                priority: priority,
                port: port,
                ttl: ttl,
                weight: weight,
                flags: flags,
                tag: tag
            });
        });
        return request.body;
    }

    static async getDNSRecord(domain, recordID) {
        const request = await got(`${domainsUrl}/${domain}/records/${recordID}`, {
            json: true,
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        });
        return request.body;
    }

    static async deleteDNSRecord(domain, recordID) {
        const request = await got(`${domainsUrl}/${domain}/records/${recordID}`, {
            json: true,
            method: 'DELETE',
            headers: {
                Authorization: authorization
            }
        });
        return request.statusCode === 204;
    }
}

module.exports = DNS;
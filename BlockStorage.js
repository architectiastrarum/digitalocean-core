const got = require('got');
const endpoint = 'volumes';
const Global = require('./Global');
const url = Global.url;
const volumesUrl = `${url}/${endpoint}`;
const authorization = `Bearer ${Global.token}`;

class BlockStorage {
    static async getAllVolumes(page = 1) {
        const request = await got(volumesUrl, {
            json: true,
            query: {
                page: page
            },
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        });
        return request.body;
    }

    static async createVolume(sizeGigabytes, name, description, region, snapshot_id, filesystem_type, filesystem_label, tags) {
        const request = await got(volumesUrl, {
            json: true,
            body: !!snapshot_id ? {
                size_gigabytes: sizeGigabytes,
                name: name,
                description: description,
                region: region,
                snapshot_id: snapshot_id,
                filesystem_type: filesystem_type,
                filesystem_label: filesystem_label,
                tags: tags
            } : {
                    size_gigabytes: sizeGigabytes,
                    name: name,
                    description: description,
                    region: region,
                    // snapshot_id: snapshot_id,
                    filesystem_type: filesystem_type,
                    filesystem_label: filesystem_label,
                    tags: tags
                },
            method: 'POST',
            headers: {
                Authorization: authorization
            }
        });
        return request.body;
    }
    static async deleteVolumeByName(name, region) {
        const request = await got(volumesUrl, {
            json: true,
            query: {
                name: name,
                region: region
            },
            method: 'DELETE',
            headers: {
                Authorization: authorization
            }
        });
        return request.statusCode;
    }

}

module.exports = BlockStorage;
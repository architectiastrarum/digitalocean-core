module.exports = {
    'version' : 'v2',
    'baseURL' :  'https://api.digitalocean.com',
    'token': process.env.API_TOKEN || '',
};
module.exports.url = `${module.exports.baseURL}/${module.exports.version}`;
module.exports.timeDilationCoefficient = 1.0;//timeDilationCoefficient is a number from 0 to 1 that is the percent global slowdown
module.exports.getTiDiInterval = function (interval) {
    return interval + (interval * module.exports.timeDilationCoefficient);
};
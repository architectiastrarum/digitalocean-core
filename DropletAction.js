/*
 * Copyright (c) 2019. Challstrom. All rights reserved.
 */

const got = require('got');
const endpoint = 'droplets';
const Global = require('./Global');
const url = Global.url;
const dropletsUrl = `${url}/${endpoint}`;
const authorization = `Bearer ${Global.token}`;

class DropletAction {
    static async performActionOnDroplet(dropletID, action) {
        const request = await got(`${dropletsUrl}/${dropletID}/actions`, {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            body: {
                type : action
            }
        }).catch((err) => {
            console.error(err);
        });
        return request.body;
    }

    static async performActionByTag(tag, action) {
        const request = await got(`${dropletsUrl}/${dropletID}/actions`, {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            query: {
                tag_name: tag
            },
            body: {
                type : action
            }
        }).catch((err) => {
            console.error(err);
        });
        return request.body;
    }

    static async getAction(dropletID, actionID) {
        const request = await got(`${dropletsUrl}/${dropletID}/actions/${actionID}`, {
            json: true,
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        }).catch((err) => {
            console.error(err);
        });
        return request.body;
    }

    static async rebuildDroplet(dropletID, slug) {
        const request = await got(`${dropletsUrl}/${dropletID}/actions`, {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            body: {
                type : 'rebuild',
                image : slug
            }
        }).catch((err) => {
            console.error(err);
        });
        return request.body;
    }

    static async rebootDroplet(dropletID) {
        return await this.performActionOnDroplet(dropletID, 'reboot');
    }

    static async powerOffDroplet(dropletID) {
        return await this.performActionOnDroplet(dropletID, 'power_off');
    }

    static async rebootByTag(tag) {
        return await this.performActionOnDroplet(dropletID, 'reboot');
    }

    static async powerOffByTag(tag) {
        return await this.performActionOnDroplet(dropletID, 'power_off');
    }

}

module.exports = DropletAction;

const got = require('got');
const endpoint = 'actions';
const Global = require('./Global');
const url = Global.url;
const actionsUrl = `${url}/${endpoint}`;
const authorization = `Bearer ${Global.token}`;

class Action {
    static async getAllActions(page = 1) {
        const request = await got(actionsUrl, {
            json: true,
            query: {
                page: page
            },
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        });
        return request.body;
    }

    static async getAction(actionID) {
        const request = await got(`${actionsUrl}/${actionID}`, {
            json: true,
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        });
        return request.body.action;
    }
}

module.exports = Action;
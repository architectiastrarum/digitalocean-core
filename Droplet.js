const got = require('got');
const endpoint = 'droplets';
const Global = require('./Global');
const url = Global.url;
const dropletsUrl = `${url}/${endpoint}`;
const authorization = `Bearer ${Global.token}`;

class Droplet {
    static async createDroplet(name, region, size, image, ssh_keys, backups, ipv6, user_data, private_networking, volumes, tags) {
        const request = await got(dropletsUrl, {
            json: true,
            method: 'POST',
            headers: {
                Authorization: authorization
            },
            body: {
                name: name,
                region: region,
                size: size,
                image: image,
                ssh_keys: ssh_keys,
                backups: backups,
                ipv6: ipv6,
                user_data: user_data,
                private_networking: private_networking,
                volumes: volumes,
                tags: tags
            }
        }).catch((err) => {
            console.log(err);
            console.log('=======================');
            console.log({
                name: name,
                region: region,
                size: size,
                image: image,
                ssh_keys: ssh_keys,
                backups: backups,
                ipv6: ipv6,
                user_data: user_data,
                private_networking: private_networking,
                volumes: volumes,
                tags: tags
            });
        });
        return request.body;
    }

    static async getDropletByID(id) {
        const request = await got(`${dropletsUrl}/${id}`, {
            json: true,
            method: 'GET',
            headers: {
                Authorization: authorization
            },
        });
        return request.body;
    }

    //god damn callbacks.
    static waitForDroplet(id, cb, interval=5000) {
        interval = Global.getTiDiInterval(interval);
        let count = 0;
        const it = setInterval(() => {
            if (count > 4) throw Error(`Droplet was not created after ${interval/1000} seconds and 4 tries!`);
            const droplet = Droplet.getDropletByID(id);
            if (!droplet.droplet.locked) {
                cb();
                it.clearInterval();
                return true;
            }
            count++;
        }, interval);
    }

    static async getAllDroplets(page=1) {
        const request = await got(dropletsUrl, {
            json: true,
            query: {
                page: page
            },
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        });
        return request.body;
    }

    static async deleteDropletByID(id) {
        const request = await got(`${dropletsUrl}/${id}`, {
            json: true,
            method: 'DELETE',
            headers: {
                Authorization: authorization
            },
        });
        return request.statusCode === 204;
    }
}

module.exports = Droplet;
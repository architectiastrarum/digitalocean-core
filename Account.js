const got = require('got');
const endpoint = 'account';
const Global = require('./Global');
const url = Global.url;
const accountsUrl = `${url}/${endpoint}`;
const authorization = `Bearer ${Global.token}`;
class Account {
    //primary request
    static async getAccountInformation() {
        const request = await got(accountsUrl, {
            json: true,
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        });
        return request.body;
    }
    //helpers
    static async getDropletLimit() {
        const accountInfo = (await this.getAccountInformation()).account;
        return accountInfo.droplet_limit;
    }

    static async getFloatingIPLimit() {
        const accountInfo = (await this.getAccountInformation()).account;
        return accountInfo.floating_ip_limit;
    }

    static async getEmail() {
        const accountInfo = (await this.getAccountInformation()).account;
        return accountInfo.email;
    }

    static async getUUID() {
        const accountInfo = (await this.getAccountInformation()).account;
        return accountInfo.uuid;
    }

    static async getEmailVerified() {
        const accountInfo = (await this.getAccountInformation()).account;
        return accountInfo.email_verified;
    }

    static async getStatus() {
        const accountInfo = (await this.getAccountInformation()).account;
        return accountInfo.status;
    }

    static async getStatusMessage() {
        const accountInfo = (await this.getAccountInformation()).account;
        return accountInfo.status_message;
    }
}

module.exports = Account;
/*
 * Copyright (c) 2019. Challstrom. All rights reserved.
 */

const got = require('got');
const endpoint = 'domains';
const Global = require('./Global');
const url = Global.url;
const domainsUrl = `${url}/${endpoint}`;
const authorization = `Bearer ${Global.token}`;

class Domain {
    static async getAllDomains(page = 1) {
        const request = await got(domainsUrl, {
            json: true,
            query: {
                page: page
            },
            method: 'GET',
            headers: {
                Authorization: authorization
            }
        });
        return request.body;
    }
}

module.exports = Domain;
const assert = require('assert');
const BlockStorage = require('../BlockStorage');

describe('BlockStorage', function () {
    describe('#getAllVolume()', function () {
        it('should get all volumes on page 1', async function () {
            const volumes = await BlockStorage.getAllVolumes();
            // console.log(volumes);
        });
    });
    //ASSUMPTIONS
    //deleteVolumeByName() is working properly, if it is not, then trash volumes will be added and must be deleted manually
    describe('#createVolume()', function () {
        this.timeout(10000);
        const testVolumeID = Math.floor(Math.random() * 1000);
        const name = `testVolume-${testVolumeID}`;
        it('should create a volume with the specified attributes.', async function () {
            const volumeCreated = await BlockStorage.createVolume(10, name, 'this is a unit test droplet.', 'nyc1', 0, 'ext4', 'testfs', ['TEST']).catch((err) => {console.error(err)});
            // console.log(volumeCreated);
        });
        after(async () => {
            this.timeout(5000);
            await BlockStorage.deleteVolumeByName(name, 'nyc1');
        });
    });
});
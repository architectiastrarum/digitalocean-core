const assert = require('assert');
const Action = require('../Action');
//ASSUMPTIONS
//1. The account associated with the given token has greater than 2 page of actions
describe('Action', () => {
    describe('#getAllActions()', function () {
        it('should get all actions on page 1.', async () => {
            const actions = await Action.getAllActions();
            assert(!!actions || actions.meta.total === 0, 'actions was not set');
            assert(actions.links.pages.next.includes('page=2'));
        });
        it('should get all actions on page 2.', async () => {
            const actions = await Action.getAllActions(2);
            assert(!!actions || actions.meta.total === 0, 'actions was not set');
            assert(actions.links.pages.next.includes('page=3'));
        });
    });
    describe('#getAction()', function () {
        it('should get an action given actionID', async () => {
            const lastAction = (await Action.getAllActions()).actions.pop();
            const actionRequested = await Action.getAction(lastAction.id);
            assert.deepStrictEqual(actionRequested, lastAction);
        });
    });
});
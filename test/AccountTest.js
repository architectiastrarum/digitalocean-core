const assert = require('assert');
const Account = require('../Account');
describe('Account', () => {
    describe('#getAccountInformation()', () => {
        it('should get all generic information about the Digital Ocean account the API key is registered to.', async () => {
            const accountInfo = (await Account.getAccountInformation()).account;
            assert('droplet_limit' in accountInfo, 'droplet_limit not present in accountInfo\n' + JSON.stringify(accountInfo, null, 4));
            assert('floating_ip_limit' in accountInfo, 'floating_ip_limit not present in accountInfo\n' + JSON.stringify(accountInfo, null, 4));
            assert('volume_limit' in accountInfo, 'volume_limit not present in accountInfo\n' + JSON.stringify(accountInfo, null, 4));
            assert('email' in accountInfo, 'email not present in accountInfo\n' + JSON.stringify(accountInfo, null, 4));
            assert('uuid' in accountInfo, 'uuid not present in accountInfo\n' + JSON.stringify(accountInfo, null, 4));
            assert('email_verified' in accountInfo, 'email_verified not present in accountInfo\n' + JSON.stringify(accountInfo, null, 4));
            assert('status' in accountInfo, 'status not present in accountInfo\n' + JSON.stringify(accountInfo, null, 4));
            assert('status_message' in accountInfo, 'status_message not present in accountInfo\n' + JSON.stringify(accountInfo, null, 4));
        });
    });
    describe('#getDropletLimit()', () => {
        it('should get the integer droplet_limit field from the DO account.', async () => {
            const dropletLimit = await Account.getDropletLimit();
            assert(Number.isSafeInteger(dropletLimit), 'droplet_limit was not set or was not an integer\n' + JSON.stringify(dropletLimit, null, 4));
        });

    });
    describe('#getFloatingIPLimit()', () => {
        it('should get the integer floating_ip_limit field from the DO account.', async () => {
            const floatingIPLimit = await Account.getFloatingIPLimit();
            assert(Number.isSafeInteger(floatingIPLimit), 'floatingIPLimit was not set or was not an integer\n' + JSON.stringify(floatingIPLimit, null, 4));
        });

    });
    describe('#getEmail()', () => {
        it('should get the string email field from the DO account.', async () => {
            const email = await Account.getEmail();
            const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            assert(emailRegex.test(String(email).toLowerCase()), 'email was not a valid email format\n' + JSON.stringify(email, null, 4));
        });
    });
    describe('#getUUID()', () => {
        it('should get the string uuid field from the DO account.', async () => {
            const uuid = await Account.getUUID();
            assert(!!uuid, 'uuid was not set');
        });

    });
    describe('#getEmailVerified()', () => {
        it('should get the bool email_verified field from the DO account.', async () => {
            const emailVerified = await Account.getEmailVerified();
            assert(!!emailVerified, 'emailVerified was not set');
        });

    });
    describe('#getStatus()', () => {
        it('should get the string status field from the DO account.', async () => {
            const status = await Account.getStatus();
            assert(['active', 'locked', 'warning'].includes(status), 'status was not set or was not a valid DO status [\'active\', \'locked\', \'warning\']\n' + JSON.stringify(status, null, 4));
        });
    });
    describe('#getStatusMessage()', () => {
        it('should get the string status_message field from the DO account.', async () => {
            const statusMessage = await Account.getStatusMessage();
            assert(!!statusMessage || statusMessage === '', 'status_message was not set\n' + JSON.stringify(statusMessage, null, 4));
        });

    });
});
/*
 * Copyright (c) 2019. Challstrom. All rights reserved.
 */

const assert = require('assert');
const DropletAction = require('../DropletAction');

describe('DropletAction', function () {
    describe('#rebootDroplet()', function () {
        it('should reboot the droplet with the given dropletID', async function () {
            const action = await DropletAction.rebootDroplet('');
            console.log(action);
        });
    });
});


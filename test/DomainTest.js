/*
 * Copyright (c) 2019. Challstrom. All rights reserved.
 */

const assert = require('assert');
const Domain = require('../Domain');

describe('Domain', function () {
    describe('#getAllDomains()', function () {
        it('should get all domains on page 1', async function () {
            const domains = await Domain.getAllDomains();
            console.log(domains);
        });
    });
});
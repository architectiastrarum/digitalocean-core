/*
 * Copyright (c) 2019. Challstrom. All rights reserved.
 */

const assert = require('assert');
const DNS = require('../DNS');
const domain = 'challstrom.com';
describe('DNS', function () {
    let createRecords = [], getRecord, deleteRecord;
    const random = Math.floor(Math.random() * 1000);
    before(async () => {
        getRecord = await DNS.createDNSRecord(domain, 'A', `getrecord${random}`, '127.0.0.1');
        deleteRecord = await DNS.createDNSRecord(domain, 'A', `deleterecord${random}`, '127.0.0.1');
    });
    after(() => {
        createRecords.forEach((record) => {
           DNS.deleteDNSRecord(domain, record.domain_record.id);
        });
        DNS.deleteDNSRecord(domain, getRecord.domain_record.id);
        try {
            DNS.deleteDNSRecord(domain, deleteRecord.domain_record.id);
        } catch (e) {
            //ignored
        }
    });
    describe('#getAllDNSRecords()', function () {
        it('should get all domains on page 1', async function () {
            const dnsRecords = await DNS.getAllDNSRecords(domain);
            assert(dnsRecords.domain_records.map((record) => record.id).includes(50303893), 'dnsRecords did not include nameserver 1, records most likely were not returned correctly.\n' + JSON.stringify(dnsRecords, null, 4));
        });
    });
    describe('#createDNSRecord()', function () {
        this.timeout(10000);
        it('should create a DNS record with the predefined values', async function () {
            createRecords.push(await DNS.createDNSRecord(domain, 'A', `creatednsecord-${random}-A`, '127.0.0.1'));
            createRecords.push(await DNS.createDNSRecord(domain, 'AAAA', `creatednsecord-${random}-AAAA`, '::1'));
            createRecords.push(await DNS.createDNSRecord(domain, 'CAA', `creatednsecord-${random}-CAA`, 'pki.goog.', null, null, 1800, null, 0, 'issue'));
            createRecords.push(await DNS.createDNSRecord(domain, 'CNAME', `creatednsecord-${random}-CNAME`, `creatednsecord-${random}-A.`));
            createRecords.push(await DNS.createDNSRecord(domain, 'MX', `creatednsecord-${random}-MX`, '127.0.0.1', 0));
            createRecords.push(await DNS.createDNSRecord(domain, 'TXT', `creatednsecord-${random}-TXT`, 'autotest'));
            createRecords.push(await DNS.createDNSRecord(domain, 'SRV', `_sip._tcp.example.com`, 'test.example.com', 0, 10, 1800, 0));
            createRecords.push(await DNS.createDNSRecord(domain, 'NS', `creatednsecord-${random}-NS`, '127.0.0.1'));
            console.log(createRecords);
        });
    });
    describe('#getDNSRecord()', function () {
        it('should get the DNS record with the given ID', async function () {
            const record = await DNS.getDNSRecord(domain, getRecord.domain_record.id);
            assert(record.domain_record.type === 'A', 'getRecord reported incorrect record type\n' + JSON.stringify(record, null, 4));
            assert(record.domain_record.name === `getrecord${random}`, 'getRecord reported incorrect name\n' + JSON.stringify(record, null, 4));
            assert(record.domain_record.data === '127.0.0.1', 'getRecord reported incorrect data\n' + JSON.stringify(record, null, 4));
            assert(record.domain_record.ttl === 1800, 'getRecord reported incorrect ttl\n' + JSON.stringify(record, null, 4));
        });
    });
    describe('#deleteDNSRecord()', function () {
        it('should delete the DNS record with the given ID', async function () {
            const success = await DNS.deleteDNSRecord(domain, deleteRecord.domain_record.id);
            assert(success);
        });
    });
});
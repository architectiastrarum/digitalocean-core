/*
 * Copyright (c) 2019. Challstrom. All rights reserved.
 */

const assert = require('assert');
const Droplet = require('../Droplet');

describe('Droplet', function () {
    this.timeout(10000);
    let createDroplet, getDroplet, deleteDroplet;
    const random = Math.floor(Math.random() * 1000);
    before(async () => {
        getDroplet = await Droplet.createDroplet(`testGetDroplet-${random}`, 'nyc3', 's-1vcpu-1gb', 'debian-9-x64', null, false, false, null, false, null, ['test']);
        deleteDroplet = await Droplet.createDroplet(`testDeleteDroplet-${random}`, 'nyc3', 's-1vcpu-1gb', 'debian-9-x64', null, false, false, null, false, null, ['test']);
    });
    //WARNING - after is not firing for some reason, need to remove errant droplets by hand
    after(function () {
        setTimeout(()=> {
            try {
                Droplet.deleteDropletByID(createDroplet.droplet.id);
            } catch (e) {
                console.log(e);
            }
            try {
                Droplet.deleteDropletByID(getDroplet.droplet.id);
            } catch (e) {
                console.log(e);
            }
            try {
                Droplet.deleteDropletByID(deleteDroplet.droplet.id);
            } catch (e) {
                console.log(e);
            }
        }, 20000);
    });
    describe('#getAllDroplets()', function () {
        it('should get all droplets on page 1', async function () {
            const droplets = await Droplet.getAllDroplets();
            console.log(droplets);
        });
    });
    describe('#getDropletByID()', function () {
        it('should get the droplet with the specified ID', async function () {
            const droplet = await Droplet.getDropletByID(getDroplet.droplet.id);
            console.log(droplet);
        });
    });
    describe('#createDroplet()', function () {
        this.timeout(21000);
        it('should create a droplet with the specified parameters and wait for it to be created', async function () {
            createDroplet = await Droplet.createDroplet(`testCreateDroplet-${random}`, 'nyc3', 's-1vcpu-1gb', 'debian-9-x64', null, false, false, null, false, null, ['test']);
            Droplet.waitForDroplet(Droplet.waitForDroplet(createDroplet.droplet.id, ()=>{
                const droplet = Droplet.getDropletByID(createDroplet.droplet.id);
                assert(droplet.droplet.locked === false, 'droplet was locked after waitForDroplet returned!');
                assert(droplet.droplet.status === "active", 'droplet was not "active" after waitForDroplet returned!');
                console.log(droplet);
            }));
        });
    });
    describe('#deleteDroplet()', function () {
        it('should delete the droplet with the specified ID', function () {
            Droplet.deleteDropletByID(deleteDroplet.droplet.id);
        });
    });
});